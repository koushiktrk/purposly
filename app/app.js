
'use strict';

var brandedConsole = angular.module('brandedConsole', [
	'ui.router'
	]);

brandedConsole.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/home');
    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('signIn', {
            url: '/signIn',
            templateUrl: 'components/site/partials/auth/login.html'
        })

        // Header STATES AND NESTED VIEWS ========================================
        .state('header', {
            url: '/header',
            templateUrl: 'components/site/partials/header/header.html'
        })

        // SIDEBAR HEADER VIEW  ========================================
        .state('header.campaigns', {
            url: '/campaigns',
            templateUrl: '/components/campaign/partials/campaign/sidebar.html',
            controller: 'SidebarController'
        })

        // SIDEBAR HEADER VIEW  ========================================
        .state('header.editor', {
            url: '/editor',
            templateUrl: '/components/editor/partials/sidebar.html',
            controller: 'SidebarController'
        })
        
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit       
        });
        
}]);